<?php
/**
 * Created by vagrant.
 * User: vagrant
 * Date: 5/12/2020
 * Time: 10:03 PM
 */

?>

<select class="tutor-ws">
	<option value="x" class="hidden">
		<?php echo esc_html__('Select Action', 'iii-notepad'); ?>
	</option>
	<option value="n">
		<?php echo esc_html__('New', 'iii-notepad'); ?>
	</option>
	<option value="tc">
		<?php echo esc_html__('Test Camera', 'iii-notepad'); ?>
	</option>
	<option value="tm">
		<?php echo esc_html__('Test Mic', 'iii-notepad'); ?>
	</option>
	<option value="uw">
		<?php echo esc_html__('Open Worksheet', 'iii-notepad'); ?>
	</option>
</select>
